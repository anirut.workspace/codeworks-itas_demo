import 'package:codework_itas/const.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String _title;

  Header(this._title);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        this._title,
        style: Theme.of(context).textTheme.title.copyWith(color: Colors.white,fontSize: 18),
      ),
    );
  }
}
