import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/poll.dart';
import 'package:codework_itas/utils/memory.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class PollMaker extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PollMakerState();
  }
}

class PollMakerState extends State<PollMaker> {
  bool _type = false;
  List<String> _data = [""];
  PollModel _result = new PollModel();
  void onChageType(bool type) => setState(() {
        this._type = type;
      });

  Widget _buildInput(String title, int index) => Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(title),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: TextField(
              onChanged: (String val) {
                this._data[index] = val;
              },
              decoration: InputDecoration(
                filled: true,
                contentPadding: EdgeInsets.all(10),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: BaseColor.BUMBLE_GUM, width: 3),
                ),
              ),
            ),
          ),
        ],
      );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("สร้าง Poll"),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.done_all),
        backgroundColor: Colors.green,
        onPressed: () async {
          this._result.setAsks(this._data);
          var mem = new Memory();
          mem.addPoll(this._result);
          Navigator.of(context).pop();
          TriggerPoll.pollT.add(this._result);
        },
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ชื่อหัวข้อ"),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: TextField(
                        onChanged: (String val) {
                          this._result.setAskTitle(val);
                        },
                        decoration: InputDecoration(
                          filled: true,
                          contentPadding: EdgeInsets.all(10),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: BaseColor.BUMBLE_GUM, width: 3),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                RadioButtonGroup(
                  labels: TYPES,
                  onSelected: (String selected) {
                    this._result.setType(selected);
                  },
                ),
                Column(
                  children: this
                      ._data
                      .asMap()
                      .map((i, v) => MapEntry(
                          i,
                          Container(
                              height: 50,
                              child: this._buildInput("คำตอบที่ ${i + 1}", i))))
                      .values
                      .toList(),
                ),
                IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () {
                    setState(() {
                      this._data.add("");
                    });
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
