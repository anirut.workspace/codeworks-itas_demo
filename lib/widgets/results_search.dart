import 'package:codework_itas/const.dart';
import 'package:flutter/material.dart';

class ResultsSearchWidget extends StatelessWidget {
  Widget _buildCardResult() => Container(
        decoration: BoxDecoration(
          border: Border.all(color: BaseColor.IMPERIAL, width: 3),
        ),
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text("ปีงบประมาณ :"),
                SizedBox(
                  width: 20,
                ),
                Text("2561"),
              ],
            ),
            Row(
              children: <Widget>[
                Text("สังกัด :"),
                SizedBox(
                  width: 20,
                ),
                Text("กระทรวงคมนาคม"),
              ],
            ),
            Row(
              children: <Widget>[
                Text("หน่วยงาน :"),
                SizedBox(
                  width: 20,
                ),
                Text("กรมจ้าท่า"),
              ],
            )
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "ผลการค้นหาทั้งหมด 10 รายการ",
          style: Theme.of(context)
              .textTheme
              .display2
              .copyWith(color: Colors.black),
        ),
        Column(
          children: List.generate(
            10,
            (i) => Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: GestureDetector(
                onTap: () => Navigator.of(context).pushNamed("/goto"),
                child: this._buildCardResult(),
              ),
            ),
          ),
        )
      ],
    );
  }
}
