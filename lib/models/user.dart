class UserModel {
  String _username;
  String _password;
  String _fname;
  String _lname;
  String _group;
  String _type;
  String _department;
  String _affiliation;
  String _email;

  UserModel();

  String get password => this._password;

  factory UserModel.fromJson(Map<String, dynamic> json) {
    var user = new UserModel();
    user
        .setAffiliation(json['affiliation'])
        .setDepartment(json['department'])
        .setEmail(json['email'])
        .setFirstname(json["fname"])
        .setLastname(json['lname'])
        .setType(json['type'])
        .setGroup(json['group'])
        .setPassword(json['password'])
        .setUsername(json['username']);
    return user;
  }

  Map<String, dynamic> toJson() => {
        "affiliation": this._affiliation,
        "department": this._department,
        "email": this._email,
        "fname": this._fname,
        "lname": this._lname,
        "type": this._type,
        "group": this._group,
        "username": this._username,
        "password": this._password
      };
  UserModel setUsername(username) {
    this._username = username;
    return this;
  }

  UserModel setPassword(password) {
    this._password = password;
    return this;
  }

  UserModel setFirstname(name) {
    this._fname = name;
    return this;
  }

  UserModel setLastname(name) {
    this._lname = name;
    return this;
  }

  UserModel setEmail(name) {
    this._email = name;
    return this;
  }

  UserModel setAffiliation(name) {
    this._affiliation = name;
    return this;
  }

  UserModel setDepartment(name) {
    this._department = name;
    return this;
  }

  UserModel setType(name) {
    this._type = name;
    return this;
  }

  UserModel setGroup(name) {
    this._group = name;
    return this;
  }

  String get username => this._username;
  String get email => this._email;
  String get type => this._type;
  String get affiliation => this._affiliation;
  String get department => this._department;
  String get group => this._group;
  String get fname => this._fname;
  String get lname => this._lname;
}
