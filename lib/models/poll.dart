class PollModel {
  String askTitle;
  List<dynamic> asks;
  String type;

  PollModel() {}

  PollModel setAskTitle(String val) {
    this.askTitle = val;
    return this;
  }

  PollModel setAsks(List<dynamic> val) {
    this.asks = val;
    return this;
  }

  PollModel setType(String val) {
    this.type = val;
    return this;
  }

  factory PollModel.fromJson(Map<String, dynamic> json) {
    var poll = new PollModel();
    poll.setAsks(json['asks']);
    poll.setAskTitle(json['ask_title']);
    poll.setType(json['type']);
    return poll;
  }

  Map<String, dynamic> toJson() => {
        "asks": this.asks,
        "ask_title": this.askTitle,
        "type": this.type,
      };
}
