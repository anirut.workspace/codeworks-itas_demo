import 'package:flutter/widgets.dart';

abstract class Reporting {
  Map<String, dynamic> toJsonTH();
  String getDataByKey(String key);
}

class ReportingOitModel extends Reporting {
  String department;
  int sent;
  int saved;
  int withoutSave;
  Color color;

  ReportingOitModel(
      {this.department, this.saved, this.sent, this.withoutSave, this.color});

  @override
  Map<String, dynamic> toJsonTH() => {
        "กระทรวง": this.department,
        "จำนวนที่บันทึกส่งแล้ว": this.sent,
        "จำนวนที่ยังไม่บันทึกส่ง": this.withoutSave,
        "จำนวนการบันทึก": this.saved,
      };

  String getDataByKey(String key) {
    switch (key) {
      case "saved":
        return this.saved.toString();
      case "withoutSave":
        return this.withoutSave.toString();
      case "sent":
        return this.sent.toString();
      case "department":
        return this.department.toString();
      default:
        return "";
    }
  }
}

class ReportingIitModel extends Reporting {
  String department;
  int explored;
  int unExplore;

  ReportingIitModel({
    this.department,
    this.explored,
    this.unExplore,
  });

  @override
  String getDataByKey(String key) {
    switch (key) {
      case "department":
        return this.department;
      case "explored":
        return this.explored.toString();
      case "unExplore":
        return this.unExplore.toString();
      default:
        return "";
    }
  }

  @override
  Map<String, dynamic> toJsonTH() => {
        "กระทรวง": this.department,
        "หน่วยงานที่มีการสำรวจแล้ว": this.explored,
        "หน่วยงานที่ยังไม่มีการสำรวจ": this.unExplore
      };
}
