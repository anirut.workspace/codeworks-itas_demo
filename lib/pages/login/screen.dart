import 'package:codework_itas/const.dart';
import 'package:codework_itas/pages/login/widgets/button_login.dart';
import 'package:codework_itas/pages/login/widgets/form_login.dart';
import 'package:codework_itas/utils/memory.dart';
import 'package:codework_itas/utils/user.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  String _username = "";
  String _password = "";

  bool loading = false;

  void _login() async {
    this._formkey.currentState.save();
    var mem = new Memory();
    var logged = await mem.login(this._username, this._password);
    if (logged) {
      User.user = await mem.getUserInCache();
      Navigator.of(context).pushReplacementNamed("/main");
    } else {
      this._formkey.currentState.reset();
    }
  }

  void handlePassword(String password) {
    this._password = password;
  }

  void handleUsername(String username) {
    this._username = username;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  ClipPath(
                    clipper: ClipperPath(false),
                    child: Container(
                      height: MediaQuery.of(context).size.height * .33,
                      width: double.infinity,
                      color: BaseColor.IMPERIAL.withOpacity(.8),
                    ),
                  ),
                  ClipPath(
                    clipper: ClipperPath(true),
                    child: Container(
                      height: MediaQuery.of(context).size.height * .33,
                      width: double.infinity,
                      color: BaseColor.IMPERIAL.withOpacity(.8),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 50,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Center(
                          child: Image.asset(
                        "assets/imgs/logo.png",
                        fit: BoxFit.cover,
                        width: 200,
                        height: 300,
                      )),
                    ),
                    FormLoginWidget(
                        _formkey, this.handlePassword, this.handleUsername),
                    SizedBox(
                      height: 20,
                    ),
                    this.loading
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : GestureDetector(
                            onTap: () {
                              setState(() {
                                this.loading = true;
                              });
                            },
                            child: ButtonLoginWidget(this._login))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ClipperPath extends CustomClipper<Path> {
  bool _bottom = false;

  ClipperPath(this._bottom);

  @override
  Path getClip(Size size) {
    var path = new Path();
    if (this._bottom) {
      path.moveTo(size.width, size.height);
      path.lineTo(size.width, 0);
      path.lineTo(0, size.height / 2);
      path.lineTo(0, size.height);
    } else {
      path.lineTo(0, size.height);
      path.lineTo(size.width, size.height / 2 + 10);
      path.lineTo(size.width, 0);
    }
    // path.lineTo(0, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
