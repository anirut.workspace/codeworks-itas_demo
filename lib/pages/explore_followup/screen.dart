import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/followup.dart';
import 'package:codework_itas/pages/common/explore_visualization/widgets/table.dart';
import 'package:codework_itas/widgets/appbar.dart';
import 'package:codework_itas/widgets/dropdown.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';
import '../../const.dart' as constant;

List<String> _readyOrNot = ["บันทึกพร้อมส่ง", "ตรวจสอบแล้ว"];

List<FollowupModel> _mock = [
  new FollowupModel(
    affilation: constant.AFFILLATIONS()[1],
    department: constant.DEPARTMENTS()[1],
    eit: 20,
    oit: _readyOrNot[1],
    iit: 34,
  ),
  new FollowupModel(
    affilation: constant.AFFILLATIONS()[1],
    department: constant.DEPARTMENTS()[0],
    eit: 51,
    oit: _readyOrNot[0],
    iit: 123,
  ),
  new FollowupModel(
    affilation: constant.AFFILLATIONS()[2],
    department: constant.DEPARTMENTS()[1],
    eit: 51,
    oit: _readyOrNot[1],
    iit: 61,
  ),
  new FollowupModel(
    affilation: constant.AFFILLATIONS()[3],
    department: constant.DEPARTMENTS()[0],
    eit: 220,
    oit: _readyOrNot[0],
    iit: 534,
  ),
  new FollowupModel(
    affilation: constant.AFFILLATIONS()[4],
    department: constant.DEPARTMENTS()[1],
    eit: 123,
    oit: _readyOrNot[0],
    iit: 556,
  ),
];

class FollowUpPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FollowUpPageState();
  }
}

class FollowUpPageState extends State<FollowUpPage> {
  bool _toggle1 = false;
  bool _toggle2 = false;
  bool _toggle3 = false;
  bool _isLoading = false;
  bool _isShowing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("ติดตามแบบสำรวจ"),
      ),
      drawer: DrawerME(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 0),
                child: Column(
                  children: <Widget>[
                    DropDownWidget("ปีงบประมาณ",
                        new List<String>.from(constant.YEARS_BUDGET())),
                    SizedBox(
                      height: 10,
                    ),
                    DropDownWidget(
                        "สังกัด", new List<String>.from(constant.AFFILLATIONS())),
                    SizedBox(
                      height: 10,
                    ),
                    DropDownWidget("หน่วยงาน",
                        new List<String>.from(constant.DEPARTMENTS())),
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: CheckboxListTile(
                      controlAffinity: ListTileControlAffinity.leading,
                      value: this._toggle1,
                      onChanged: (bool val) {
                        setState(() {
                          this._toggle1 = val;
                        });
                      },
                      title: Text("OIT",style:TextStyle(fontSize: 10)),
                    ),
                  ),
                  Expanded(
                    child: CheckboxListTile(
                      controlAffinity: ListTileControlAffinity.leading,
                      value: this._toggle2,
                      onChanged: (bool val) {
                        setState(() {
                          this._toggle2 = val;
                        });
                      },
                      title: Text("IIT",style:TextStyle(fontSize: 10)),
                    ),
                  ),
                  Expanded(
                    child: CheckboxListTile(
                      value: this._toggle3,
                      controlAffinity: ListTileControlAffinity.leading,
                      onChanged: (bool val) {
                        setState(() {
                          this._toggle3 = val;
                        });
                      },
                      title: Text("EIT",style:TextStyle(fontSize: 10)),
                    ),
                  ),
                ],
              ),
              Container(
                width: double.infinity,
                child: FlatButton.icon(
                  padding: EdgeInsets.all(5),
                  onPressed: () {
                    setState(() {
                      this._isLoading = true;
                    });
                    Future.delayed(Duration(milliseconds: 400)).then((_) {
                      setState(() {
                        this._isShowing = true;
                        this._isLoading = !true;
                      });
                    });
                  },
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 30,
                  ),
                  color: BaseColor.IMPERIAL,
                  label: Text(
                    "ค้นหา",
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              this._isLoading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : this._isShowing
                      ? TableWidget(
                          _mock.map((d) => d.toJsonTH()).toList(), true)
                      : Container()
            ],
          ),
        ),
      ),
    );
  }
}
