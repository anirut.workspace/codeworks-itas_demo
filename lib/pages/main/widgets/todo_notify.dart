import 'package:codework_itas/pages/main/widgets/box_content.dart';
import 'package:flutter/material.dart';

List d = [
  {
    "title": "สำรวจความคิดเห็นผู้มีส่วนได้ส่วนเสียภายใน",
    "desc": "ดำเนินการสำรวจภายใน เพื่อให้สามารถดำเนินการในขั้นตอนต่อไปไม่ได้"
  },
  {
    "title": "แบบสอบประเมิณเรียบร้อยแล้ว",
    "desc": "แบบประเมินเมื่อวันที่ 20 กันยายน 2561 ได้ทำการตรวจสอบเรียบร้อยแล้ว"
  }
];

class TodoNotifyWidget extends StatelessWidget {
  Widget _buildTodo(BuildContext context, String title, String desc) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            height: 30,
            width: 30,
            child: Image.asset("assets/imgs/bell.png"),
          ),
          Container(
            height: 80,
            width: MediaQuery.of(context).size.width * .8,
            child: ListTile(
              title: Text(title,style: TextStyle(
                fontSize: 16
              ),),
              isThreeLine: true,
              subtitle: Text(desc,style: TextStyle(
                fontSize: 14
              ),),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BoxContentWidget(
        180,
        MediaQuery.of(context).size.width - 20,
        "รายการแจ้งเตือนวันนี้",
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Container(
            height: 200,
            padding: EdgeInsets.all(0),
            width: double.infinity,
            child: ListView.separated(
              separatorBuilder: (c,s) => Divider(),
              itemCount: d.length,
              scrollDirection: Axis.vertical,
              controller: ScrollController(initialScrollOffset: 15),
              itemBuilder: (BuildContext context, int index) => this._buildTodo(
                context,
                d[index]['title'],
                d[index]['desc'],
              ),
            ),
          ),
        ));
  }
}
