import 'package:codework_itas/pages/main/widgets/box_content.dart';
import 'package:flutter/material.dart';

class NewsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BoxContentWidget(
      390,
      MediaQuery.of(context).size.width * .95,
      "ข่าวสาร",
      Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  border: Border.all(width: 3, color: Colors.black)),
              child: Image.asset(
                "assets/imgs/news.jpg",
                fit: BoxFit.contain,
              ),
            ),
            Text(
              "เลขาธิการคณะกรรมการ ป.ช.ช. แถลงข่าว 4 คดี 06/08/2019",
              style: Theme.of(context)
                  .textTheme
                  .display2
                  .copyWith(color: Colors.black, fontSize: 16),
            ),
            Text(
              "คณะกรรมการ ป.ป.ช. จึงมีเหตุอันควรสงสัยว่า นายพนม ศรศิลป์ มีพฤติกรรมร่ำรวยผิดปกติ จึงได้มีคำสั่งแต่งตั้งคณะกรรมการใต่สวน",
              style: Theme.of(context)
                  .textTheme
                  .display1
                  .copyWith(color: Colors.black, fontSize: 12),
            )
          ],
        ),
      ),
    );
  }
}
