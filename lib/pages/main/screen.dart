import 'package:codework_itas/const.dart';
import 'package:codework_itas/pages/main/widgets/news.dart';
import 'package:codework_itas/pages/main/widgets/todo_notify.dart';
import 'package:codework_itas/widgets/appbar.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    as cld;

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerME(),
      appBar: AppBar(
        title: Header("หน้าหลัก"),
      ),
      body: Padding(
        padding: EdgeInsets.all(8),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints bc) =>
              SingleChildScrollView(
            child: Column(
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1,
                  child: cld.CalendarCarousel(
                    markedDatesMap: cld.EventList<Event>(events: {
                      DateTime(2019, 9, 23): [
                        Event(
                            dot: Icon(
                              Icons.notification_important,
                              color: BaseColor.IMPERIAL,
                              size: 10,
                            ),
                            date: DateTime(2019, 9, 23, 8),
                            icon: Icon(
                              Icons.warning,
                              color: Colors.red,
                            ),
                            title: "แถลงข่าว")
                      ]
                    }),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TodoNotifyWidget(),
                NewsWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
