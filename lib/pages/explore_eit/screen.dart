import 'package:codework_itas/const.dart';
import 'package:codework_itas/widgets/appbar.dart';
import 'package:codework_itas/widgets/dropdown.dart';
import 'package:codework_itas/widgets/results_search.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:codework_itas/widgets/year_input.dart';
import 'package:flutter/material.dart';
import '../../const.dart' as constant;

class EitPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return EitPageState();
  }
}

class EitPageState extends State<EitPage> {
  bool _isLoading = false;

  bool _isShowing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("แบบประเมิน (EIT) "),
      ),
      drawer: DrawerME(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
               DropDownWidget(
                        "ปีงบประมาณ", new List<String>.from(constant.YEARS_BUDGET())),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 0,vertical: 0),
                child: Column(
                  children: <Widget>[
                 DropDownWidget(
                        "สังกัด", new List<String>.from(constant.AFFILLATIONS())),
                    SizedBox(
                      height: 10,
                    ),
                    DropDownWidget("หน่วยงาน",
                        new List<String>.from(constant.DEPARTMENTS())),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                child: FlatButton.icon(
                  padding: EdgeInsets.all(5),
                  onPressed: () {
                    setState(() {
                      this._isLoading = true;
                    });
                    Future.delayed(Duration(milliseconds: 400)).then((_) {
                      setState(() {
                        this._isShowing = true;
                        this._isLoading = !true;
                      });
                    });
                  },
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 30,
                  ),
                  color: BaseColor.IMPERIAL,
                  label: Text(
                    "ค้นหา",
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              this._isLoading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : this._isShowing ? ResultsSearchWidget() : Container()
            ],
          ),
        ),
      ),
    );
  }
}
