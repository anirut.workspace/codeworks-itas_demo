import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/poll.dart';
import 'package:codework_itas/utils/memory.dart';
import 'package:codework_itas/widgets/appbar.dart';
import 'package:codework_itas/widgets/poll_card.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';

class PollPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PollPageState();
  }
}

class PollPageState extends State<PollPage> {
  List<PollModel> _polls = [];

  bool _loading = true;

  @override
  void initState() {
    super.initState();
    Memory().polls.then((p) {
      p.forEach((poll) {
        this._polls.add(poll);
      });
      setState(() {
        this._loading = true;
      });
    });

    TriggerPoll.pollT.listen((poll) {
      this._polls.add(poll);
      setState(() {
        this._loading = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("Poll"),
      ),
      drawer: DrawerME(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).pushNamed("/poll/maker"),
        child: Icon(Icons.create),
      ),
      body: Padding(
        padding: EdgeInsets.all(8),
        child: SingleChildScrollView(
          child: this._loading
              ? Column(
                  children: this._polls.map((poll) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: PollCardWidget(
                        asks: poll.asks,
                        title: poll.askTitle,
                        type: poll.type,
                      ),
                    );
                  }).toList(),
                )
              : Center(
                  child: CircularProgressIndicator(),
                ),
        ),
      ),
    );
  }
}
