import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/user.dart';
import 'package:codework_itas/utils/memory.dart';
import 'package:codework_itas/utils/user.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';

class MEPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MEPageState();
  }
}

class MEPageState extends State<MEPage> {
  UserModel user = User.user;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildTextField(String initValue, String type, Function handle) {
    return TextFormField(
      initialValue: initValue,
      onSaved: handle,
      readOnly: type == "username",
      decoration: InputDecoration(
        labelText: type,
        contentPadding: EdgeInsets.all(15),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(width: 3, color: BaseColor.IMPERIAL),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(width: 3, color: BaseColor.IMPERIAL),
        ),
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide(width: 3, color: BaseColor.IMPERIAL),
        ),
      ),
    );
  }

  void handleUsername(String val) {
    this.user.setUsername(val);
  }

  void handlePassword(String val) {
    this.user.setPassword(val);
  }

  void handleEmail(String val) {
    this.user.setEmail(val);
  }

  void handleDepartment(String val) {
    this.user.setDepartment(val);
  }

  void handleGroup(String val) {
    this.user.setGroup(val);
  }

  void handleAffiliation(String val) {
    this.user.setAffiliation(val);
  }

  void handleType(String val) {
    this.user.setType(val);
  }

  void handleFname(String val) {
    this.user.setFirstname(val);
  }

  void handleLname(String val) {
    this.user.setLastname(val);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("ME"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: BaseColor.PASTEL_VIOLET,
                    width: 7
                  )
                ),
                child: CircleAvatar(
                  maxRadius: 100,
                  minRadius: 50,
                  backgroundImage: AssetImage("assets/imgs/avatar.jpg"),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Form(
                key: this._formKey,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: this._buildTextField(
                              user.fname, "ชื่อ", this.handleFname),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: this._buildTextField(
                                user.lname, "นามสกุล", this.handleLname)),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: this._buildTextField(
                                user.username, "username", this.handleEmail)),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: this._buildTextField(
                                user.email, "email", this.handleEmail)),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: this._buildTextField(user.affiliation,
                                "สังกัด", this.handleAffiliation)),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: this._buildTextField(
                                user.type, "ประเภท", this.handleType)),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: this._buildTextField(
                                user.group, "กลุ่ม", this.handleGroup)),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: this._buildTextField(user.department,
                                "หน่วยงาน", this.handleDepartment)),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: <Widget>[
                        RaisedButton(
                          padding: EdgeInsets.all(10),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                          ),
                          onPressed: () {
                            Key key = new UniqueKey();
                            TextEditingController textEditingController =
                                TextEditingController();
                            TextEditingController textEditingController2 =
                                TextEditingController();
                            showDialog(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                      title: Center(
                                          child: Text(
                                        "เปลี่ยนรหัสผ่าน",
                                        style: Theme.of(context)
                                            .textTheme
                                            .title
                                            .copyWith(color: Colors.black),
                                      )),
                                      content: Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                .35,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .8,
                                        child: Column(
                                          children: <Widget>[
                                            TextField(
                                              key: key,
                                              controller: textEditingController,
                                              decoration: InputDecoration(
                                                  labelText: "รหัสเก่า"),
                                              obscureText: true,
                                            ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            TextField(
                                              obscureText: true,
                                              controller:
                                                  textEditingController2,
                                              decoration: InputDecoration(
                                                  labelText: "รหัสใหม่"),
                                              onChanged: (String val) {},
                                            ),
                                            RaisedButton(
                                              child: Text("เปลี่ยนรหัส"),
                                              onPressed: () {
                                                Memory()
                                                    .changePassword(
                                                        textEditingController
                                                            .value.text,
                                                        textEditingController2
                                                            .value.text)
                                                    .then((pass) {
                                                  if (pass) {
                                                    Navigator.of(context).pop();
                                                  } else {
                                                    textEditingController
                                                        .clear();
                                                    textEditingController2
                                                        .clear();
                                                  }
                                                });
                                              },
                                            )
                                          ],
                                        ),
                                      ),
                                    ));
                          },
                          color: Colors.yellow[900],
                          child: Text("เปลี่ยนรหัสผ่าน"),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        RaisedButton(
                          padding: EdgeInsets.all(10),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                          ),
                          onPressed: () {
                            this._formKey.currentState.save();
                            Memory().channgeInfoUser(this.user);
                            Navigator.of(context).pop();
                          },
                          color: Colors.green,
                          child: Text("แก้ไขข้อมูล"),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
