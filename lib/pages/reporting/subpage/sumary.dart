import 'package:codework_itas/const.dart';
import 'package:codework_itas/pages/common/explore_visualization/widgets/table.dart';
import 'package:codework_itas/widgets/dropdown.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

var data = [
  {
    "ปี": "2559",
    "จำนวนหน่วยงาน": "4443",
    "ผลการประเมิน": 56,
  },
  {
    "ปี": "2560",
    "จำนวนหน่วยงาน": "443",
    "ผลการประเมิน": 36,
  },
  {
    "ปี": "2561",
    "จำนวนหน่วยงาน": "3443",
    "ผลการประเมิน": 86,
  },
];

class ReportingSummarySubpage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ReportingSummarySubpageState();
  }
}

class ReportingSummarySubpageState extends State<ReportingSummarySubpage> {
  bool show = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("ผลการประเมิน"),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              DropDownWidget(
                  "ปีที่งบประมาณ", new List<String>.from(YEARS_BUDGET())),
              SizedBox(
                height: 10,
              ),
              DropDownWidget(
                  "ถึงปีที่งบประมาณ", new List<String>.from(YEARS_BUDGET())),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                color: BaseColor.MIDDLE_PURPLE,
                onPressed: () {
                  setState(() {
                    this.show = true;
                  });
                },
                child: Text(
                  "แสดง",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              !this.show
                  ? Container()
                  : Column(
                      children: <Widget>[
                        Center(
                          child: Container(
                            height: 300,
                            width: double.infinity,
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: SimpleBarChart(),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        Center(child: TableWidget(data, true))
                      ],
                    ),
            ],
          ),
        ),
      ),
    );
  }
}

/// Bar chart example

class SimpleBarChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      _createSampleData(),
      animate: false,
    );
  }

  /// Create one series with sample hard coded data.
  List<charts.Series<OrdinalSales, String>> _createSampleData() {
    final data2 =
        data.map((d) => new OrdinalSales(d['ปี'], d['ผลการประเมิน'])).toList();

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data2,
      )
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
