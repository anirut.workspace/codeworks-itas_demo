import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/history.dart';
import 'package:codework_itas/pages/common/explore_visualization/widgets/table.dart';
import 'package:codework_itas/widgets/dropdown.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';
import '../../../const.dart' as constant;


class ReportingHistoryPage extends StatelessWidget {
  HistoryModel _historyModel = new HistoryModel(
    date: "26/2/12",
    department: "sdas sa",
    desc: "loeras as is waf sa asdsa",
    ip: "192.168.1.1",
    menu: "ss ss",
    username: "azaza013",
  );

  InputDecoration _decorateInput(String label) => InputDecoration(
        labelText: label,
        contentPadding: EdgeInsets.all(10),
        fillColor: BaseColor.IMPERIAL.withOpacity(.8),
        labelStyle: TextStyle(color: Colors.white, fontSize: 20),
        filled: true,
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("รายงานประวัติการใช้งาน"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
              child: Form(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: DropDownWidget("เมนู",[]),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: TextFormField(
                            decoration: this._decorateInput("username"),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(child: DropDownWidget("สังกัด",constant.AFFILLATIONS())),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: DropDownWidget("หน่วยงาน",constant.DEPARTMENTS()),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                bottom: 30,
                top: 20,
                left: 30,
                right: 30,
              ),
              child: RaisedButton(
                onPressed: () {},
                padding: EdgeInsets.all(10),
                color: Colors.purple[900],
                child: Center(
                  child: Text(
                    "ค้นหา",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            TableWidget(
                List.generate(20, (i) => this._historyModel.toJsonTH())
                    .toList(),
                true)
          ],
        ),
      ),
    );
  }
}
