import 'package:flutter/material.dart';
import 'package:codework_itas/models/visual_explore.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class StackedHorizontalBarChart extends StatelessWidget {
  final bool animate = false;
  List<VisualExplore> explored;

  StackedHorizontalBarChart(this.explored);

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      _createSampleData(),
      animate: animate,
      barGroupingType: charts.BarGroupingType.stacked,
      vertical: false,
    );
  }

  List<charts.Series<VisualExplore, String>> _createSampleData() {
    return [
      new charts.Series<VisualExplore, String>(
        id: 'Test',
        domainFn: (VisualExplore sales, _) =>
            sales.type.length > 7 ? sales.type.substring(0, 7) : sales.type,
        measureFn: (VisualExplore sales, _) => sales.explored,
        colorFn: (VisualExplore sales, _) =>
            charts.Color.fromHex(code: "#8B5FBF"),
        data: explored,
      ),
      new charts.Series<VisualExplore, String>(
        id: 'Test2',
        domainFn: (VisualExplore sales, _) =>
            sales.type.length > 7 ? sales.type.substring(0, 7) : sales.type,
        colorFn: (VisualExplore sales, _) =>
            charts.Color.fromHex(code: "#D183C9"),
        measureFn: (VisualExplore sales, _) => sales.unExplore,
        data: explored,
      ),
    ];
  }
}
