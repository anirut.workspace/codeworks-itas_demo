import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/poll.dart';
import 'package:codework_itas/utils/memory.dart';
import 'package:codework_itas/widgets/dropdown.dart';
import 'package:codework_itas/widgets/poll_card.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';

class GoToPage extends StatefulWidget {
  @override
  _GoToPageState createState() => _GoToPageState();
}

class _GoToPageState extends State<GoToPage> {
  List<PollModel> _polls = [];
  bool _loading = false;

  @override
  void initState() {
    Memory().polls.then((p) {
      p.forEach((poll) {
        this._polls.add(poll);
      });
      setState(() {
        this._loading = true;
      });
    });
  }

  Widget _buildRow(title, value) => Row(
        children: <Widget>[
          Text(title + ":"),
          SizedBox(
            width: 10,
          ),
          Text(value)
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("ตอบแบบสอบถาม"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    this._buildRow("ปีงบประมาณ", "2561"),
                    SizedBox(
                      height: 10,
                    ),
                    this._buildRow("สังกัด", "กระทรวงคมนาคม"),
                    SizedBox(
                      height: 10,
                    ),
                    this._buildRow("หน่วยงาน", "กรมเจ้าท่า"),
                  ],
                ),
              ),
              Text(
                "ดัชนีความโปร่งใส",
                style: Theme.of(context)
                    .textTheme
                    .display2
                    .copyWith(color: Colors.black),
              ),
              Text(
                "ตัวชีวัด: การเปิดเผยข้อมูล",
                style: Theme.of(context)
                    .textTheme
                    .display1
                    .copyWith(color: Colors.black),
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                children: this._polls.map<Widget>((poll) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: PollCardWidget(
                      title: poll.askTitle,
                      asks: poll.asks,
                      type: poll.type,
                    ),
                  );
                }).toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
