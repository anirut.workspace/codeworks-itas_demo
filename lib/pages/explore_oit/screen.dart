import 'package:codework_itas/const.dart';
import 'package:codework_itas/widgets/appbar.dart';
import 'package:codework_itas/widgets/dropdown.dart';
import 'package:codework_itas/widgets/results_search.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../const.dart' as constant;

class OitPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OitPageState();
  }
}

class OitPageState extends State<OitPage> {
  bool _toggle1 = false;
  bool _toggle2 = false;
  bool _isLoading = false;

  bool _isShowing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerME(),
      appBar: AppBar(
        title: Header("แบบประเมิน (OIT) "),
      ),
      body: Padding(
        padding: EdgeInsets.all(8),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints bc) =>
              SingleChildScrollView(
            child: Column(
              children: <Widget>[
                 DropDownWidget(
                        "ปีงบประมาณ", new List<String>.from(constant.YEARS_BUDGET())),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 0,vertical: 0),
                  child: Column(
                    children: <Widget>[
                      DropDownWidget("สังกัด", constant.AFFILLATIONS()),
                      SizedBox(
                        height: 10,
                      ),
                      DropDownWidget("หน่วยงาน", constant.DEPARTMENTS()),
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    Container(
                      width: 130,
                      child: CheckboxListTile(
                        value: this._toggle1,
                        controlAffinity: ListTileControlAffinity.leading,
                        onChanged: (bool val) {
                          setState(() {
                            this._toggle1 = val;
                          });
                        },
                        title: Text("บันทึก"),
                      ),
                    ),
                    Expanded(
                      child: CheckboxListTile(
                        controlAffinity: ListTileControlAffinity.leading,
                        value: this._toggle2,
                        onChanged: (bool val) {
                          setState(() {
                            this._toggle2 = val;
                          });
                        },
                        title: Text("บันทึกพร้อมส่งแล้ว"),
                      ),
                    ),
                  ],
                ),
                Container(
                  width: double.infinity,
                  child: FlatButton.icon(
                    padding: EdgeInsets.all(5),
                    onPressed: () {
                      setState(() {
                        this._isLoading = true;
                      });
                      Future.delayed(Duration(milliseconds: 400)).then((_) {
                        setState(() {
                          this._isShowing = true;
                          this._isLoading = !true;
                        });
                      });
                    },
                    icon: Icon(
                      Icons.search,
                      color: Colors.white,
                      size: 30,
                    ),
                    color: BaseColor.IMPERIAL,
                    label: Text(
                      "ค้นหา",
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                this._isLoading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : this._isShowing ? ResultsSearchWidget() : Container()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
